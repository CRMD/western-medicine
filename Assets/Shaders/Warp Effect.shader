﻿Shader "Hidden/Warp Effect" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
}

SubShader
{
	Pass
	{
		ZTest Always Cull Off ZWrite Off
		Fog { Mode off }

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest 
#pragma target 3.0
#include "UnityCG.cginc"

uniform sampler2D _MainTex;

uniform float4 _MainTex_ST;

uniform float4 _MainTex_TexelSize;
uniform float _speed;
uniform float _radius;
uniform float _periods;
uniform float3 _centre;
uniform float _timeN;

struct v2f {
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
	float2 uvOrig : TEXCOORD1;
};

v2f vert (appdata_img v)
{
	v2f o;
	o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
	float2 uv = v.texcoord.xy - _centre.xy;
	o.uv = TRANSFORM_TEX(uv, _MainTex); //MultiplyUV (UNITY_MATRIX_TEXTURE0, uv);
	o.uvOrig = uv;
	return o;
}

float4 frag (v2f i) : SV_Target
{
	float2 uv = i.uvOrig + _centre;

    float umbra = 0.2F;
    float penumbra = 0.3F;
    float radiusOverTwo = _radius / 2.0F;
    float2 diffUV = uv - _centre;
    float2 direction = normalize(diffUV);
    float angle = atan2(diffUV.y, diffUV.x);
    float2 secondCentre = float2(_centre.x + radiusOverTwo, _centre.y);
    float lengthDiffUV = length(diffUV);
    float angleProg = 1.0F - (abs(angle) - umbra) / (penumbra - umbra);
    float distProg = lengthDiffUV / _radius;
    float secondDistProg = length(uv - secondCentre) / radiusOverTwo;
    
    float2 dest = uv;
    
    if (lengthDiffUV < _radius)
    {   
    	float timeSpeed = _timeN * _speed;
    	float periodsMulti = _periods * 6.28F;
    
    	float offset = (1.0F - distProg) * (sin(timeSpeed + distProg * periodsMulti) + 
    										sin(timeSpeed + secondDistProg * periodsMulti));
        
        if (abs(angle) < penumbra && abs(angle) > umbra)
		{
            offset *= angleProg;
			
		}
        
        if (abs(angle) < penumbra)
		{
			dest = i.uvOrig + direction * offset;
		}	
    }
		
	return tex2D(_MainTex, dest);
}
ENDCG

	}
}

Fallback off

}