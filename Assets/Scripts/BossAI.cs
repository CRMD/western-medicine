﻿using UnityEngine;
using System.Collections;

public class BossAI : MonoBehaviour {

	EnemyState mState;

	private NavMeshAgent mAgent;					//The NavMesh agent component
	private Transform mPlayer;						//Reference to the player
	private Animator mAnim;							//The animation/animator
	
	public float mMaxHealth = 0.1f;					//The maximum health
	private float mCurHealth;						//The current health
	
	public float mSpeed;							//Movement Speed
	public float mAttackRange = 2.5f;				//Attack Range
	
	public float mNewPointTime;						//Time before a new point is set along it's path
	private float mElapsedTime;						//Time from last point set
	
	private bool mAware;							//If the enemy is aware of the player
	public float mAwarenessRange;					//How close the player can get before they're noticed

	public float mWanderRange;						//How far from the starting point can the boss wander
	public float mAttackTimer;						//How long before the boss can shoot again
	float mLastAttack;

	public Gun mGun;

	private Vector3 mOldPos;


	float mPlayerMaxHealth;				//
	float mPlayerHealth;				//
	int mPlayerMoney;					//
	int mPlayerPotions;					//
	int mLevel;							//
	string mMoneyKey = "Sheriff Money";			//
	string mHealthKey = "Sheriff Health";		//
	string mCurHealthKey = "Sherif CurHealth";	//
	string mPotionsKey = "Sheriff Potions";		//
	string mLevelKey = "Sheriff Level";	

	// Use this for initialization
	void Start () {
		mPlayer = GameObject.Find ("Player").transform;
		mAgent = gameObject.GetComponent<NavMeshAgent> ();
		mAnim = gameObject.GetComponentInChildren<Animator> ();
		
		if(mMaxHealth <= 0)
		{
			mMaxHealth = 1.0f;
		}
		
		mCurHealth = mMaxHealth;
		mAgent.speed = mSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		mElapsedTime += Time.deltaTime;
		mLastAttack += Time.deltaTime;

		Vector3 toPlayer = mPlayer.transform.position - transform.position;
		Vector3 leadShot = mPlayer.gameObject.GetComponent<CharacterController3D>().mMoveDir;
		leadShot += mPlayer.position;
//		Debug.DrawRay (transform.position, (toPlayer + leadShot), Color.red, 0f);
		Debug.DrawLine(transform.position, mPlayer.position, Color.red, 0f);
		Debug.DrawLine (transform.position, leadShot, Color.blue, 0f);
		Debug.DrawLine (mPlayer.position, leadShot, Color.yellow, 0f);

		if(!mAware)
		{
			if(Vector3.Distance(mPlayer.position, transform.position) < mAwarenessRange)
			{
				mAware = true;
				mLastAttack = 0.0f;
			}
			else
			{
				if(transform.parent)
				{
					if(mElapsedTime >= mNewPointTime)
					{
						mAgent.SetDestination(Roam (transform.parent.position));
						mElapsedTime = 0.0f;
					}
				}
			}
		}
		else
		{
			if(mLastAttack > mAttackTimer)
			{
				mLastAttack = 0.0f;
				//toPlayer = mPlayer.transform.position - transform.position;
				//leadShot = new Vector3(mPlayer.rigidbody.velocity.x, mPlayer.rigidbody.velocity.y, mPlayer.rigidbody.velocity.z);

				transform.LookAt(leadShot);
			}
		}
		UpdateAnimations ();
	}

	void LateUpdate()
	{
		mOldPos = transform.position;
	}

	public void TakeDamage(float dmg)
	{
		mCurHealth -= dmg;
		if(mCurHealth <= 0)
		{
			mPlayerMaxHealth = mPlayer.gameObject.GetComponent<CharacterStats>().mPlayerMaxHealth;
			mPlayerMoney = mPlayer.gameObject.GetComponent<CharacterStats>().mPlayerMoney;
			mPlayerPotions = mPlayer.gameObject.GetComponent<CharacterStats>().mPlayerPotions;
			mPlayerHealth = mPlayer.gameObject.GetComponent<CharacterStats>().mPlayerHealth;
			mLevel = mPlayer.gameObject.GetComponent<CharacterStats>().mLevel;

			Destroy(gameObject);
		}
		mAware = true;
	}

	void UpdateAnimations()
	{
		if(!mAware)
		{
			if(mOldPos.x < transform.position.x - 0.5f * Time.deltaTime
				   || mOldPos.x > transform.position.x + 0.5f * Time.deltaTime
				   || mOldPos.z < transform.position.z - 0.5f * Time.deltaTime
				   || mOldPos.z > transform.position.z + 0.5f * Time.deltaTime)
			{
				mState = EnemyState.walking;
			}
			else
			{
				mState = EnemyState.idle;
			}
		}
		else
		{
			mState = EnemyState.attacking;
		}
		switch (mState)
		{
		case EnemyState.attacking:
			mAnim.SetBool ("shooting", true);
			mAnim.SetBool ("walking", false);
			break;
		case EnemyState.idle:
			mAnim.SetBool ("walking", false);
			break;
		case EnemyState.walking:
			mAnim.SetBool ("walking", true);
			break;
		}
	}

	Vector3 Roam(Vector3 basePoint)			//Aimlessly roam around basePoint
	{
		Vector3 retVec = basePoint;
		float variance = mWanderRange;
		
		retVec += new Vector3(Random.Range(-variance, variance), 0.0f, Random.Range(-variance, variance));

		return retVec;
	}

	void OnDestroy()
	{
		if (Camera.main)
			Camera.main.SendMessage ("FadeToBlack", SendMessageOptions.DontRequireReceiver);

		PlayerPrefs.SetFloat(mHealthKey, mPlayerMaxHealth);
		PlayerPrefs.SetInt(mMoneyKey, mPlayerMoney);
		PlayerPrefs.SetInt(mPotionsKey, mPlayerPotions);
		PlayerPrefs.SetFloat(mCurHealthKey, mPlayerHealth);
		PlayerPrefs.SetInt(mLevelKey, mLevel);
	}
}
