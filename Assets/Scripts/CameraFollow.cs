﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform mPlayer;
	public Vector3 mOffset;

	public bool mShopping = false;
	Vector3 mOldPos;
	Vector3 mOldRot;
	public Transform mShopTransform;
	float startTime;
	public float mLerpSpeed;
	Vector3 mOffsetPos;
	bool mLerping;

	private float mShopRange = 5.0f;

	bool mFadeToBlack;
	float mDecay = 50.0f;

	
	public string mLevelKey = "Sheriff Level";	

	// Use this for initialization
	void Start () {
		if(!mPlayer)
		{
			mPlayer = GameObject.Find("Player").transform;
		}
		transform.position = new Vector3((mPlayer.position.x + mOffset.x), (mPlayer.position.y + mOffset.y), (mPlayer.position.z + mOffset.z));
		transform.LookAt(mPlayer);

		mOffsetPos = new Vector3((mPlayer.position.x + mOffset.x), (mPlayer.position.y + mOffset.y), (mPlayer.position.z + mOffset.z)) - transform.forward;
		mOldPos = mOffsetPos;
		mOldRot = transform.rotation.eulerAngles;
		
		startTime = Time.time;
		mShopping = true;
		mLerping = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(mFadeToBlack)
		{
//			RenderSettings.fogStartDistance = Mathf.Lerp(299.99f, -10000, Time.deltaTime * mDecay);
			mDecay *= 1.1f;
			RenderSettings.fogStartDistance -= Time.deltaTime * mDecay;
			if(RenderSettings.fogStartDistance < -10000.0f)
			{
				if(PlayerPrefs.GetInt(mLevelKey) > 4)
				{
					Application.LoadLevel("PlayableLoopEnd");
				}
				else
				{
					Application.LoadLevel(Application.loadedLevel);
				}
			}
		}

		mOldPos = mOffsetPos = new Vector3((mPlayer.position.x + mOffset.x), (mPlayer.position.y + mOffset.y), (mPlayer.position.z + mOffset.z)) - transform.forward;
		if(!mLerping)
		{
			if(!mShopping)
			{
				MouseLead();
//				Time.timeScale = 1.0f;
				if(Vector3.Distance(mPlayer.position, mShopTransform.position) < mShopRange && Input.GetKeyDown(KeyCode.E))
				{
					mOldPos = mOffsetPos;
					mOldRot = transform.rotation.eulerAngles;
					
					startTime = Time.time;
					mShopping = true;
					mLerping = true;
				}
			}
			else
			{
//				Time.timeScale = 0.0f;
				if(Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Escape))
				{
					startTime = Time.time;
					mShopping = false;
					mLerping = true;
					mShopTransform.parent.gameObject.BroadcastMessage("StopGesturing", SendMessageOptions.DontRequireReceiver);
				}
			}
		}
		else
		{
//			Time.timeScale = 1.0f;
			if(Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Escape))
			{
				startTime = Time.time;
				mShopping = false;
				mLerping = true;
				mShopTransform.parent.gameObject.BroadcastMessage("StopGesturing", SendMessageOptions.DontRequireReceiver);
			}
			ShopLerp ();
		}
	}

	bool CloseEnough(Vector3 vec1, Vector3 vec2)
	{
		float diff = 0.0001f;
		if(vec1.x < vec2.x + diff && vec1.x > vec2.x - diff
		   && vec1.y < vec2.y + diff && vec1.y > vec2.y - diff
		   && vec1.z < vec2.z + diff && vec1.z > vec2.z - diff)
		{
			return true;
		}
		return false;
	}

	public void FadeToBlack()
	{
		mFadeToBlack = true;
	}

	void ShopLerp()
	{
		if(!mShopping)
		{
			transform.position = Vector3.Lerp(mShopTransform.position, mOldPos, ((Time.time - startTime) * mLerpSpeed) / Vector3.Distance (mShopTransform.position, mOldPos));
			transform.rotation = Quaternion.Slerp(mShopTransform.rotation, Quaternion.Euler(mOldRot), ((Time.time - startTime) * mLerpSpeed) / Vector3.Distance (mShopTransform.position, mOldPos));
			
			if(CloseEnough(mOldPos, transform.position) && CloseEnough(mOldRot, transform.rotation.eulerAngles))
			{
				mLerping = false;
				mPlayer.gameObject.SendMessage("StopInvulnerable", SendMessageOptions.DontRequireReceiver);
			}
		}
		else
		{
			transform.position = Vector3.Lerp(mOldPos, mShopTransform.position, ((Time.time - startTime) * mLerpSpeed) / Vector3.Distance (mShopTransform.position, mOldPos));
			transform.rotation = Quaternion.Slerp(Quaternion.Euler(mOldRot), mShopTransform.rotation, ((Time.time - startTime) * mLerpSpeed) / Vector3.Distance (mShopTransform.position, mOldPos));
			
			if(CloseEnough(transform.position, mShopTransform.position) && CloseEnough(transform.rotation.eulerAngles, mShopTransform.rotation.eulerAngles))
			{
				mLerping = false;
				mShopTransform.parent.gameObject.BroadcastMessage("StartGesture", SendMessageOptions.DontRequireReceiver);
				mPlayer.gameObject.SendMessage("StartInvulnerable", SendMessageOptions.DontRequireReceiver);
			}
		}
	}

	void MouseLead()
	{
		//transform.position = mOffsetPos;
		Vector3 MousePos = new Vector3((Input.mousePosition.x / Screen.currentResolution.width), 0f, (Input.mousePosition.y / Screen.currentResolution.height));
		MousePos = new Vector3(MousePos.x * -transform.forward.x, 0f, MousePos.z * transform.forward.z);
		transform.position = Vector3.Lerp(transform.position, mOffsetPos + MousePos * 7, Time.deltaTime * 5f);
	}

	void OnGUI()
	{
		if(!mShopping && Vector3.Distance(mPlayer.position, mShopTransform.position) < mShopRange)
		{
			GUI.Label(new Rect(400.0f, 10.0f, 100.0f, 20.0f), "Press 'E' to Shop");
		}
	}
}
