﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {
	
	private EnemyState mState = EnemyState.idle;	//Current State
	
	private NavMeshAgent mAgent;					//The NavMesh agent component
	private Transform mPlayer;						//Reference to the player
	private Animator mAnim;						//The animation/animator
	
	public float mMaxHealth = 0.1f;					//The maximum health
	private float mCurHealth;						//The current health
	public float mDamage;							//The base damage the enemy does to the player
	
	public float mSpeed;							//Movement Speed
	public float mAttackRange = 2.5f;				//Attack Range

	public float mNewPointTime;						//Time before a new point is set along it's path
	private float mElapsedTime;						//Time from last point set
	
	public GameObject mMoneyPouch;					//The Money Pouch that is dropped when the enemy dies

	public float mStumbleTime;						//The base amount of time the enemy  will stumble for after each attack
	private float mStumble;							//How long the enemy has stumbled for

	private bool mAware;							//If the enemy is aware of the player
	public float mAwarenessRange;					//How close the player can get before they're noticed
	private bool mCharging;							//If the enemy is charging
	public float mChargeRange;						//How close the player can get before the enemy starts charging at them
	public float mChargeCap;						//The maximum bonus to damage/stagger time
	private float mChargeTime;						//The amount of time the enemy has been charging their attack
	public float mChargeDamageBonus;				//Bonus damage scaled by mChargeTime
	public float mChargeSpeedBonus;					//Bonus speed scaled by mChargeTime
	public float mChargeStaggerBonus;				//Bonus time added to the stagger after attack
	
	public AudioClip[] passiveSounds;
	public AudioClip[] attackSounds;
	public AudioClip[] noticeSounds;

//	Rigidbody[] mRagDoll;
	float mFallTime;

	void Start () {
		mPlayer = GameObject.Find ("Player").transform;
		mAgent = gameObject.GetComponent<NavMeshAgent> ();
		mAnim = gameObject.GetComponentInChildren<Animator> ();

		if(mMaxHealth <= 0)
		{
			mMaxHealth = 1.0f;
		}
		
		mCurHealth = mMaxHealth;
		mAgent.speed = mSpeed;
	}
	
	void Update () {
		mAgent.speed = mSpeed + (mChargeTime * mChargeSpeedBonus);

		UpdateTimes();

		if(mFallTime > 3)
		{
			transform.position -= Vector3.up * Time.deltaTime;
			return;
		}

		if(Camera.main.GetComponent<CameraFollow>().mShopping)
		{
			return;
		}

		if(mStumble <= 0)
		{
			if(!mAware)
			{
				if(Vector3.Distance(transform.position, mPlayer.position) < mAwarenessRange)
				{
					if(transform.parent)
					{
						transform.parent.gameObject.BroadcastMessage("NoticePlayer", SendMessageOptions.DontRequireReceiver);
						
						if (noticeSounds.Length != 0)
							AudioSource.PlayClipAtPoint (noticeSounds[Random.Range (0, noticeSounds.Length - 1)], transform.position);
						else
							Debug.Log("No Enemy Notice Audio");
					}
					else
					{
						NoticePlayer();
					}
				}
				else
				{
					if(transform.parent)
					{
						if(mElapsedTime >= mNewPointTime)
						{
							mAgent.SetDestination(Roam (transform.parent.position));
							mState = EnemyState.wander;
							mElapsedTime = 0;
						}
					}
				}
			}
			else
			{
				if(!mCharging)
				{
					if(Vector3.Distance(transform.position, mPlayer.position) < mChargeRange)
					{
						mState = EnemyState.charging;
						mCharging = true;
						if(mElapsedTime >= mNewPointTime)
						{
							if(mCurHealth > 0)
							{
								mAgent.SetDestination(mPlayer.position);
								mElapsedTime = 0;
							}
						}
					}
					if(mCurHealth > 0)
					mAgent.SetDestination(FindPoint (mPlayer.position));
				}
				else
				{
					if(Vector3.Distance(transform.position, mPlayer.position) < mAttackRange)
					{
						Attack ();
						
						if (attackSounds.Length != 0)
							AudioSource.PlayClipAtPoint (attackSounds[Random.Range (0, attackSounds.Length - 1)], transform.position);
						else
							Debug.Log("No Enemy Attack Audio");
					}
					else
					{
						if(mCurHealth > 0)
						mAgent.SetDestination(mPlayer.position);
					}
				}
			}
		}
	}

	void UpdateTimes()
	{
		if(mCurHealth < 0)
			mFallTime += Time.deltaTime;

		if(mCharging)
		{
			mChargeTime += Time.deltaTime;
			if(mChargeTime > mChargeCap)
			{
				mChargeTime = mChargeCap;
			}
		}
		mStumble -= Time.deltaTime;
		mElapsedTime += Time.deltaTime;
	}
	
	void FixedUpdate()
	{
		if(mCurHealth > 0)
		{
			if(mAgent.remainingDistance <= mAgent.stoppingDistance)
			{
				mState = EnemyState.idle;
			}
			else if(!mCharging)
			{
				if(mAware == true)
				{
					mState = EnemyState.walking;
				}
				else
				{
					mState = EnemyState.wander;
				}
			}
			if(Vector3.Distance(mPlayer.position, transform.position) <= mAttackRange)
			{
				mState = EnemyState.attacking;
			}
			
			UpdateAnimations();
		}
	}

	Vector3 Roam(Vector3 basePoint)			//Aimlessly roam around basePoint
	{
		Vector3 retVec = basePoint;
		float variance = 10.0f;

		retVec += new Vector3(Random.Range(-variance, variance), 0.0f, Random.Range(-variance, variance));

		return retVec;
	}

	Vector3 FindPoint(Vector3 targetPos)		//Wander toward targetPos
	{
		float variance = 5.0f;
		float dist = Vector3.Distance (mPlayer.position, transform.position);

		if(variance > dist)
		{
			variance = dist/Random.Range(1.0f, 2.0f);
		}
		dist /= 2;
		
		Vector3 retvec = mPlayer.position + ((mPlayer.position - transform.position)/2);
		retvec += new Vector3 (Random.Range(-variance, variance), 0, Random.Range(-variance, variance));

		return retvec;
	}

	public void NoticePlayer()
	{
		transform.parent = null;
		mAware = true;
	}
	
	public void TakeDamage(float dmg)
	{
		mCurHealth -= dmg;
		if(mCurHealth <= 0)
		{
			Instantiate(mMoneyPouch, transform.position, Quaternion.identity);
			//		Rigidbody[] mRagDoll = gameObject.GetComponentsInChildren<Rigidbody>();
			//
			//		foreach(Rigidbody ragDoll in mRagDoll)
			//		{
			//			ragDoll.isKinematic = false;
			//		}
			mAgent.enabled = false;
			mAnim.enabled = false;
			transform.GetComponent<CapsuleCollider>().enabled = false;
			Destroy (gameObject, 10.0f);
		}

		if(!mAware)
		{
			if(transform.parent)
			{
				transform.parent.gameObject.BroadcastMessage("NoticePlayer", SendMessageOptions.DontRequireReceiver);
			}
			else
			{
				NoticePlayer();
			}
		}
	}

	public void Attack()
	{
		float attackRadius = 0.5f;
		RaycastHit[] hits = Physics.SphereCastAll(transform.position, attackRadius, transform.forward, mAttackRange);

		for (int i = 0; i < hits.Length; ++i)
		{
			if(hits[i].collider.name == "Player")
			{
				float damage = mDamage + (mChargeTime * mChargeDamageBonus);
				hits[i].collider.gameObject.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
				break;
			}
		}
		mStumble = mStumbleTime + (mChargeTime * mChargeStaggerBonus);
		mCharging = false;
		mState = EnemyState.idle;
		mChargeTime = 0.0f;
	}

	void UpdateAnimations()
	{
		switch (mState)
		{
		case EnemyState.attacking:
			mAnim.SetBool("Attacking", true);
			mAnim.SetBool("Idle", false);
			mAnim.SetBool("Following", false);
			mAnim.SetBool("Charging", false);
			mAnim.SetBool("Wander", false);
			break;
		case EnemyState.idle:
			mAnim.SetBool("Idle", true);
			mAnim.SetBool("Attacking", false);
			mAnim.SetBool("Following", false);
			mAnim.SetBool("Charging", false);
			mAnim.SetBool("Wander", false);
			break;
		case EnemyState.walking:
			mAnim.SetBool("Following", true);
			mAnim.SetBool("Idle", false);
			mAnim.SetBool("Attacking", false);
			mAnim.SetBool("Charging", false);
			mAnim.SetBool("Wander", false);
			break;
		case EnemyState.charging:
			mAnim.SetBool("Charging", true);
			mAnim.SetBool("Idle", false);
			mAnim.SetBool("Following", false);
			mAnim.SetBool("Attacking", false);
			mAnim.SetBool("Wander", false);
			break;
		case EnemyState.wander:
			mAnim.SetBool("Wander", true);
			mAnim.SetBool("Idle", false);
			mAnim.SetBool("Following", false);
			mAnim.SetBool("Attacking", false);
			mAnim.SetBool("Charging", false);
			break;
		}
	}
}

public enum EnemyState
{
	idle,
	attacking,
	walking,
	charging,
	wander
}