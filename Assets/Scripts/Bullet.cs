﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	float mSpeed;
	float mDamage;
	GameObject mOwner;

	// Use this for initialization
	// Update is called once per frame
	void Update () {
		transform.Translate (new Vector3 (0, 0, mSpeed * Time.deltaTime));//transform.forward * mSpeed * Time.deltaTime);
	}

	void OnTriggerEnter(Collider col)
	{
		if (!col.gameObject.name.Contains("Bullet") && !col.gameObject.name.Contains(mOwner.name))
		{
			col.gameObject.SendMessage("TakeDamage", mDamage, SendMessageOptions.DontRequireReceiver);
			
			GameObject soundObj = new GameObject();
			soundObj.transform.position = transform.position;
			soundObj.transform.parent = Camera.main.transform;
			soundObj.AddComponent<AudioSource>();
			soundObj.audio.rolloffMode = AudioRolloffMode.Linear;
			soundObj.audio.volume = 0.05F;
			if (CharacterController3D.instance.ricoSounds.Length != 0)
				soundObj.audio.clip = CharacterController3D.instance.ricoSounds [Random.Range (0, CharacterController3D.instance.ricoSounds.Length - 1)];
			else
				Debug.Log("No Bullet Rico Audio");
			soundObj.audio.Play();

			Destroy (gameObject);
			Destroy(soundObj, 5F);
		}
	}
	void OnCollisionEnter(Collision col)
	{
		Debug.Log (mOwner.name);
		Debug.Log (col.gameObject.name);
		if (!col.gameObject.name.Contains("Bullet") && !col.gameObject.name.Contains(mOwner.name))
		{
			col.gameObject.SendMessage("TakeDamage", mDamage, SendMessageOptions.DontRequireReceiver);
			Destroy (gameObject);
		}
	}

	public void SetValues (BulletProperties bp)
	{
		mSpeed = bp.mSpeed;
		mDamage = bp.mDamage;
		mOwner = bp.mOwner;

		Destroy(gameObject, bp.mKillTime);
	}
}
