﻿using UnityEngine;
using System.Collections;

public class CharacterStats : MonoBehaviour {

	public float mPlayerMaxHealth;				//
	public float mPlayerMaxHealthDefault;		//
	public float mPlayerHealth;					//
	public int mPlayerMoney;					//
	public int mPlayerPotions;					//

	public string mMoneyKey = "Sheriff Money";			//
	public string mHealthKey = "Sheriff Health";		//
	public string mCurHealthKey = "Sherif CurHealth";	//
	public string mPotionsKey = "Sheriff Potions";		//
	public string mLevelKey = "Sheriff Level";			//

	public bool debugWipe;						//

	public float mHealthDecay;					//
	public float mHealthRegen;					//
	public float mMaxHealthRegen;				//

	bool mInvulnerable;							//Invulnerability

	public int mLevel;
	
	public AudioClip[] deathSounds;
	public AudioClip[] goreSounds;

	void Awake()
	{
		if(debugWipe)
		{
			PlayerPrefs.DeleteKey(mMoneyKey);
			PlayerPrefs.DeleteKey(mHealthKey);
			PlayerPrefs.DeleteKey(mPotionsKey);
			PlayerPrefs.DeleteKey(mCurHealthKey);
			PlayerPrefs.DeleteKey(mLevelKey);
		}
	}

	// Use this for initialization
	void Start () {
		if(PlayerPrefs.HasKey(mHealthKey))
		{
			mPlayerMaxHealth = PlayerPrefs.GetFloat(mHealthKey);
		}
		else
		{
			PlayerPrefs.SetFloat(mHealthKey, mPlayerMaxHealth);
		}
		if(PlayerPrefs.HasKey(mMoneyKey))
		{
			mPlayerMoney = PlayerPrefs.GetInt(mMoneyKey);
		}
		else
		{
			PlayerPrefs.SetInt(mMoneyKey, mPlayerMoney);
		}
		if(PlayerPrefs.HasKey(mPotionsKey))
		{
			mPlayerPotions = PlayerPrefs.GetInt(mPotionsKey);
		}
		else
		{
			PlayerPrefs.SetInt(mPotionsKey, mPlayerPotions);
		}
		if(PlayerPrefs.HasKey(mLevelKey))
		{
			mLevel = PlayerPrefs.GetInt(mLevelKey);
		}
		else
		{
			PlayerPrefs.SetInt(mLevelKey, mLevel);
		}
		if(PlayerPrefs.HasKey(mCurHealthKey))
		{
			mPlayerHealth = PlayerPrefs.GetFloat(mCurHealthKey);
		}
		else
		{
			mPlayerHealth = mPlayerMaxHealth;
			PlayerPrefs.SetFloat(mCurHealthKey, mPlayerHealth);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(mPlayerHealth < mPlayerMaxHealth)
		{
			mPlayerHealth += mHealthRegen * Time.deltaTime;
		}
		if(mPlayerMaxHealth < mPlayerMaxHealthDefault)
		{
			mPlayerMaxHealth += mMaxHealthRegen * Time.deltaTime;
		}
	}

	void OnDestroy()
	{

	}

	public void StartInvulnerable()
	{
		mInvulnerable = true;
	}
	public void StopInvulnerable()
	{
		mInvulnerable = false;
	}

	public void TakeDamage(float dmg)
	{
		if(mInvulnerable)
		{
			return;
		}
			
		AudioSource.PlayClipAtPoint(goreSounds [Random.Range (0, goreSounds.Length - 1)], transform.position);
		
		mPlayerHealth -= dmg;
		if(mPlayerHealth <= 0)
		{
			AudioSource.PlayClipAtPoint(deathSounds [Random.Range (0, deathSounds.Length - 1)], Camera.main.transform.position);
			gameObject.SendMessage("Die");
		}
	}

	public bool BuyPotion(int cost)
	{
		if(mPlayerMoney >= cost)
		{
			mPlayerMoney -= cost;
			mPlayerPotions++;
			return true;
		}
		return false;
	}

	public void AddMoney(int money)
	{
		mPlayerMoney += money;
	}

	public void ConsumePotion()
	{
		if(mPlayerPotions > 0 && mPlayerHealth != mPlayerMaxHealth)
		{
			mPlayerMaxHealth -= mHealthDecay;
			mPlayerHealth = mPlayerMaxHealth;
			mPlayerPotions--;
		}
	}

	void OnGUI()
	{
		GUI.Label(new Rect(10.0f, 10.0f, 200.0f, 20.0f), "Health: " + mPlayerHealth.ToString() + "/" + mPlayerMaxHealth.ToString() + "/" + mPlayerMaxHealthDefault.ToString());
		GUI.Label(new Rect(10.0f, 30.0f, 100.0f, 20.0f), "Potions: " + mPlayerPotions.ToString());
		GUI.Label(new Rect(10.0f, 50.0f, 100.0f, 20.0f), "Money: " + mPlayerMoney.ToString());
	}
}
