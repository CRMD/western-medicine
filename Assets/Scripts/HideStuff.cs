﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HideStuff : MonoBehaviour
{
	
	// parallel arrays
	public List<GameObject> invisibleStuff = new List<GameObject>();
	public List<Material> oldMats = new List<Material>();
	public Dictionary<GameObject,Material> oldMaterials = new Dictionary<GameObject,Material>();
	public GameObject toThing;
	public Material toMat;
	
	void Start () {
		
	}
	
	void OnPreRender()
	{
		
		// restore all old materials
		foreach (var pair in oldMaterials) 
		{
			GameObject key = pair.Key;
			Material value = pair.Value;
			key.renderer.material = value;
		}
		
		oldMaterials.Clear();
		
		Vector3 dir =  toThing.transform.position - Camera.main.transform.position;
		Vector3.Normalize (dir);
		
		Ray rayToPlayer = new Ray(Camera.main.transform.position, dir);
	//RaycastHit[] rayHit = Physics.RaycastAll(rayToPlayer, Vector3.Distance(toThing.transform.position, Camera.main.transform.position) - 0.0F);
		RaycastHit[] rayHit = Physics.SphereCastAll(rayToPlayer, 1, Vector3.Distance(toThing.transform.position, Camera.main.transform.position) - 1.5F);
		Debug.DrawRay(Camera.main.transform.position, dir, Color.red, .01f);
		
		List<RaycastHit> orderedHits = new List<RaycastHit> ();
		/*
		for (int i = 0; i < rayHit.Length; i++) {
			for (int j = 0; j < rayHit.Length - 1; j++) {
				if (Vector3.Distance(rayHit[j].transform.position, Camera.main.transform.position) > Vector3.Distance(rayHit[j + 1].transform.position, Camera.main.transform.position)) {
					RaycastHit temp = rayHit[j + 1];
					rayHit[j + 1] = rayHit[j];
					rayHit[j] = temp;
				}
			}
		}*/
		
		//Debug.Log("-----");
		for (int i = 0; i < rayHit.Length; i++)
		{
			// get all children of hit object
			
			
			//Debug.Log(rayHit[i].collider.gameObject.transform.root.name);
			
			if (rayHit[i].collider.gameObject.transform.root.name == "Player" || 
			    rayHit[i].collider.gameObject.transform.root.name == "Shop" ||
			    rayHit[i].collider.gameObject.transform.root.name == "Enemy" )
				continue;
			
			
			Renderer[] mats = rayHit[i].collider.gameObject.transform.root.GetComponentsInChildren<Renderer>();
			for (int j = 0; j < mats.Length; j++)
			{
				if(!oldMaterials.ContainsKey(mats[j].gameObject))
				{
					oldMaterials[mats[j].gameObject]=mats[j].gameObject.renderer.material;
					mats[j].gameObject.renderer.material=toMat;
				}
			}
			//invisibleStuff.Add(mats[i].gameObject);
			//oldMats.Add(mats[i].gameObject.renderer.material);
			// add all hit children to lists
			//for (int j = 0; j < mats.Length; j++)
			//{
			//	mats[j].gameObject.renderer.material = toMat;
			//}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	/*
	// restore all old materials
	for (int i = 0; i < invisibleStuff.Count; i++)
	{
		Renderer[] mats = invisibleStuff[i].transform.root.GetComponentsInChildren<Renderer>();
		for (int j = 0; j < mats.Length; j++)
		{
			mats[j].gameObject.renderer.material = oldMats[i];
		}
	}
	
	invisibleStuff.Clear ();
	oldMats.Clear ();
	
	Debug.Log (rayHit.Length);
	// for each hit object from ray
	for (int i = 0; i < rayHit.Length; i++)
	{
		// get all children of hit object
		Renderer[] mats = rayHit[i].collider.gameObject.transform.root.GetComponentsInChildren<Renderer>();
		
		invisibleStuff.Add(mats[i].gameObject);
		oldMats.Add(mats[i].gameObject.renderer.material);
		// add all hit children to lists
		for (int j = 0; j < mats.Length; j++)
		{
			mats[j].gameObject.renderer.material = toMat;
		}
	}

*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Update is called once per frame
	void Update () {
		
	}
}
