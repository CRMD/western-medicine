﻿using UnityEngine;
using System.Collections;

public class HucksterAnimator : MonoBehaviour {

	Animator mAnim;
	bool mGesturing;

	// Use this for initialization
	void Start () {
		mAnim = GetComponent<Animator>();
		if(!mAnim)
			Debug.Log("Can't find animator component on huckster, make sure an animator is attached");
	}
	
	// Update is called once per frame
	void Update () {
//		mAnim.animation["idle"].speed = 1.0f;
//		mAnim.animation["startGesture"].speed = 1.0f;
//		mAnim.animation["holdGesture"].speed = 1.0f;
//		mAnim.animation["endGesture"].speed = 1.0f;
	}

	public void StartGesture()
	{
		mGesturing = true;
		mAnim.SetBool("gesture", true);
		mAnim.SetBool("stillGesturing", false);
	}
	public void StopGesturing()
	{
		mGesturing = false;
		mAnim.SetBool("stillGesturing", false);
	}

	public void CheckGesture()
	{
		if(mGesturing)
		{
			mAnim.SetBool("stillGesturing", true);
		}
		mAnim.SetBool("gesture", false);
	}
}
