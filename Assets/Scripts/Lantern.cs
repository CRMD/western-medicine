﻿using UnityEngine;
using System.Collections;

public class Lantern : MonoBehaviour {

	
	public AudioClip[] possibleSounds;
	float currentTime = 0F;


	// Use this for initialization
	void Start () {
		currentTime = Random.Range (0.0F, 150.0F);
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;
		if (currentTime > 150F)
		{
			if (possibleSounds.Length != 0)
				GetComponent<AudioSource>().clip = possibleSounds [Random.Range (0, possibleSounds.Length - 1)];
			else
				Debug.Log("No Lantern Audio");

			GetComponent<AudioSource>().Play();
			currentTime = 0F;
		}
	}
}
