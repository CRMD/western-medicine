﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

private bool mFadeToBlack;
	float mDecay = 50.0f;


	void Update()
	{
		if(mFadeToBlack)
		{
			//			RenderSettings.fogStartDistance = Mathf.Lerp(299.99f, -10000, Time.deltaTime * mDecay);
			mDecay *= 1.1f;
			RenderSettings.fogStartDistance -= Time.deltaTime * mDecay;
			if(RenderSettings.fogStartDistance < -10000.0f)
			{
				Application.LoadLevel("PlayableLoop");
			}
		}
	}
	
	public void FadeToBlack()
	{
		mFadeToBlack = true;
	}
}
