﻿using UnityEngine;
using System.Collections;

public class SheriffAnimator : MonoBehaviour {

	public AudioClip[] possibleFootsteps;
	public GameObject soundposition;
	
	public Animator mAnimat;
	
	private int idleInt;

	public void ToggleShooting1()
	{
		SendMessageUpwards("ToggleShootAnim", 1, SendMessageOptions.DontRequireReceiver);
	}
	public void ToggleShooting2()
	{
		SendMessageUpwards("ToggleShootAnim", 2, SendMessageOptions.DontRequireReceiver);
	}
	public void ToggleShooting3()
	{
		SendMessageUpwards("ToggleShootAnim", 3, SendMessageOptions.DontRequireReceiver);
	}
	public void ToggleShooting4()
	{
		SendMessageUpwards("ToggleShootAnim", 4, SendMessageOptions.DontRequireReceiver);
	}
	public void ToggleShooting5()
	{
		SendMessageUpwards("ToggleShootAnim", 5, SendMessageOptions.DontRequireReceiver);
	}
	
	public void PlayFootstepSound()
	{
		AudioSource.PlayClipAtPoint (possibleFootsteps [Random.Range (0, possibleFootsteps.Length - 1)], soundposition.transform.position);
	}
	
	public void ResetPotion()
	{
		mAnimat.SetBool ("potion", false);
		mAnimat.StopPlayback();
	}
	public void ResetReload()
	{
		mAnimat.SetBool("reload", false);
	}
	public void ResetShooting()
	{
		mAnimat.SetBool("shooting", false);
		mAnimat.SetBool ("potion", false);
	}
	
	public void Idlizer ()
	{
		idleInt = Random.Range(1, 6);
		if(idleInt == 1)
		{
			mAnimat.SetInteger("idleType", 1);			
		}
		else
		{
			mAnimat.SetInteger("idleType", 0);
		}
	}
}
