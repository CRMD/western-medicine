﻿using UnityEngine;
using System.Collections;

public class CharacterController3D : MonoBehaviour {
	
	public static CharacterController3D instance;
	public AudioClip[] potionClickSounds;
	public AudioClip[] drinkPotionSounds;
	public AudioClip[] whizSounds;
	public AudioClip[] ricoSounds;
	
	public float mSpeed;
	public float mMaxSpeed;
	public float mDeceleration;
	Vector3 mWayPoint = Vector3.zero;
	public Vector3 mMoveDir;

	public GameObject mCurGun;

	public Animator mAnim;
	private bool mShooting = false;
	private bool mStillShooting = false;
	private Vector3 mOldPos;
	private bool mDead;

	private int mShootingStep;
	
	// Use this for initialization
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		CheckInputs();
		if(mMoveDir != Vector3.zero)
			mMoveDir.Normalize();
//		if(mCurGun.transform.parent == null)
//		{
//			mCurGun.transform.localPosition = Vector3.zero;
//			mCurGun.transform.localRotation = Quaternion.Euler(Vector3.zero);
//		}
		transform.position += (mMoveDir * mSpeed * Time.deltaTime);
		transform.LookAt(new Vector3(transform.position.x + mMoveDir.x, transform.position.y, transform.position.z + mMoveDir.z));
		UpdateAnimations ();
	}
	void LateUpdate () 
	{
		mOldPos = transform.position;
		
		if(Time.timeScale == 0)
		{
			return;
		}
		
		
		Transform bone1 = transform.Find ("HeroAnimations/SherrifPelvis/SherrifSpine1");
		Transform bone2 = transform.Find ("HeroAnimations/SherrifPelvis/SherrifSpine1/SherrifSpine2");
		Transform bone3 = transform.Find ("HeroAnimations/SherrifPelvis/SherrifSpine1/SherrifSpine2/SherrifSpine3");
		Transform bone4 = transform.Find ("HeroAnimations/SherrifPelvis/SherrifSpine1/SherrifSpine2/SherrifSpine3/SherrifSpine4");
		Transform bone5 = transform.Find ("HeroAnimations/SherrifPelvis/SherrifSpine1/SherrifSpine2/SherrifSpine3/SherrifSpine4/SherrifSpine5");
		
		//Debug.DrawLine (transform.position, transform.position + transform.forward, Color.red);
		
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		int layermask = 1 << 8;
		layermask = ~layermask;
		if (Physics.Raycast(ray, out hit, Mathf.Infinity, layermask))
		{
			Vector3 lookDirection = hit.point - transform.position;
			lookDirection.y = 0;
			lookDirection = Vector3.Normalize(lookDirection);
			//Debug.DrawLine (transform.position, transform.position + lookDirection, Color.red);
			
			Vector3 bone1forward = Vector3.Slerp(transform.forward, lookDirection, 1F / 5F);
			bone1.LookAt(bone1.position + bone1forward);
			bone1.Rotate(new Vector3(0.0f,0.0f,-90.0f));
			bone1.Rotate(new Vector3(90.0f,0.0f,0.0f));
			//Debug.DrawLine (bone1.position, bone1.position + bone1.up, Color.yellow);
			
			Vector3 bone2forward = Vector3.Slerp(bone1.up, lookDirection, 1F / 4F);
			bone2.LookAt(bone2.position + bone2forward);
			bone2.Rotate(new Vector3(0.0f,0.0f,-90.0f));
			bone2.Rotate(new Vector3(90.0f,0.0f,0.0f));
			//Debug.DrawLine (bone2.position, bone2.position + bone2.up, Color.yellow);
			
			Vector3 bone3forward = Vector3.Slerp(bone2.up, lookDirection, 1F / 3F);
			bone3.LookAt(bone3.position + bone3forward);
			bone3.Rotate(new Vector3(0.0f,0.0f,-90.0f));
			bone3.Rotate(new Vector3(90.0f,0.0f,0.0f));
			//Debug.DrawLine (bone3.position, bone3.position + bone3.up, Color.yellow);
			
			Vector3 bone4forward = Vector3.Slerp(bone3.up, lookDirection, 1F / 2F);
			bone4.LookAt(bone4.position + bone4forward);
			bone4.Rotate(new Vector3(0.0f,0.0f,-90.0f));
			bone4.Rotate(new Vector3(90.0f,0.0f,0.0f));
			//Debug.DrawLine (bone4.position, bone4.position + bone4.up, Color.yellow);
			
			Vector3 bone5forward = Vector3.Slerp(bone4.up, lookDirection, 1F / 1F);
			bone5.LookAt(bone5.position + bone5forward);
			bone5.Rotate(new Vector3(0.0f,0.0f,-90.0f));
			bone5.Rotate(new Vector3(90.0f,0.0f,0.0f));
			//Debug.DrawLine (bone5.position, bone5.position + bone5.up, Color.yellow);
		}
	}
	void CheckInputs()
	{
		mMoveDir = Vector3.zero;
		if(Camera.main.GetComponent<CameraFollow>().mShopping)
		{
			return;
		}
		if(Input.GetKey(KeyCode.W))
		{
			mMoveDir += new Vector3(0, 0, 1);
			mSpeed = mMaxSpeed;
		}
		if(Input.GetKey(KeyCode.A))
		{
			mMoveDir += new Vector3(-1, 0, 0);
			mSpeed = mMaxSpeed;
		}
		if(Input.GetKey(KeyCode.S))
		{
			mMoveDir += new Vector3(0, 0, -1);
			mSpeed = mMaxSpeed;
		}
		if(Input.GetKey(KeyCode.D))
		{
			mMoveDir += new Vector3(1, 0, 0);
			mSpeed = mMaxSpeed;
		}
		if(Input.GetKeyDown(KeyCode.Mouse0))
		{
			mCurGun.SendMessage("Fire", SendMessageOptions.DontRequireReceiver);
		}
		if(Input.GetKeyDown(KeyCode.R))
		{
			mCurGun.SendMessage("Reload", SendMessageOptions.DontRequireReceiver);
		}
		
		if(Input.GetKeyDown(KeyCode.Q) && mAnim.GetBool("potion") == false)
		{
			mAnim.SetBool ("potion", true);
			gameObject.SendMessage("ConsumePotion", SendMessageOptions.DontRequireReceiver);	
			AudioSource.PlayClipAtPoint(drinkPotionSounds [Random.Range (0, drinkPotionSounds.Length - 1)], Camera.main.transform.position);
		
		}

		if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
		{

		}
		else
		{
			if(mSpeed <= 0)
			{

			}
			else
			{
//				mSpeed -= mDeceleration;
//				Debug.Log("decelerating");
			}
		}
	}
	
	public void ToggleShootAnim(int step)
	{
		if(!mStillShooting)
		{
			mShooting = false;
			mShootingStep = step;
		}
		else
		{
			mStillShooting = false;
		}
	}

	public void Die()
	{
		mDead = true;
		Camera.main.SendMessage ("FadeToBlack");
	}

	void UpdateAnimations ()
	{
		if(mOldPos.x < transform.position.x - 0.5f * Time.deltaTime
		   || mOldPos.x > transform.position.x + 0.5f * Time.deltaTime
		   || mOldPos.z < transform.position.z - 0.5f * Time.deltaTime
		   || mOldPos.z > transform.position.z + 0.5f * Time.deltaTime)
		{
			mAnim.SetBool("walking", true);
		}
		else
		{
			mAnim.SetBool("walking", false);
		}
	
		if(mDead)
		{
			mAnim.SetBool("dead", true);
		}
	}
}
