﻿using UnityEngine;
using System.Collections;

public class StartButton : MonoBehaviour {

	public Component mHalo;

	public float mStartMaxHealth;
	public float mStartCurHealth;
	public int mStartMoney;
	public int mStartPotions;
	
	private string mMoneyKey = "Sheriff Money";
	private string mHealthKey = "Sheriff Health";
	private string mCurHealthKey = "Sherif CurHealth";
	private string mPotionsKey = "Sheriff Potions";
	private string mLevelKey = "Sheriff Level";

	// Use this for initialization
	void Start () {
		mHalo = gameObject.GetComponent ("Halo");
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnMouseOver()
	{
		if(Input.GetKeyDown(KeyCode.Mouse0))
		{
			PlayerPrefs.SetFloat(mHealthKey, mStartMaxHealth);
			PlayerPrefs.SetInt(mMoneyKey, mStartMoney);
			PlayerPrefs.SetInt(mPotionsKey, mStartPotions);
			PlayerPrefs.SetFloat(mCurHealthKey, mStartCurHealth);
			PlayerPrefs.SetInt(mLevelKey, 1);
			
			Camera.main.SendMessage("FadeToBlack", SendMessageOptions.DontRequireReceiver);
		}
		mHalo.GetType().GetProperty("enabled").SetValue(mHalo, true, null);
	}
	
	void OnMouseExit()
	{
		//		mLight.active = false;
		mHalo.GetType().GetProperty("enabled").SetValue(mHalo, false, null);
	}
	
}