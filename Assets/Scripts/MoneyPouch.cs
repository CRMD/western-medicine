﻿using UnityEngine;
using System.Collections;

public class MoneyPouch : MonoBehaviour {

	public int mValue;
	public AudioClip[] dropSounds;
	public AudioClip[] pickupSounds;

	public void SetValue(int val)
	{
		mValue = val;

		if (CharacterController3D.instance.ricoSounds.Length != 0)
		{
			GetComponent<AudioSource> ().clip = dropSounds [Random.Range (0, dropSounds.Length - 1)];
			GetComponent<AudioSource> ().rolloffMode = AudioRolloffMode.Linear;
			GetComponent<AudioSource> ().Play ();
		}
		else
			Debug.Log("No Pouch Drop Audio");

	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.name == "Player")
		{
			col.gameObject.SendMessage("AddMoney", mValue, SendMessageOptions.DontRequireReceiver);
			//GetComponent<AudioSource> ().clip = ;
			//GetComponent<AudioSource>().Play();
			if (CharacterController3D.instance.ricoSounds.Length != 0)
				AudioSource.PlayClipAtPoint(pickupSounds [Random.Range (0, pickupSounds.Length - 1)], Camera.main.transform.position);
			else
				Debug.Log("No Pouch Pickup Audio");

			Destroy(gameObject);
		}
	}
}
