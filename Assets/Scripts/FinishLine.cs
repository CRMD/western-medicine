﻿using UnityEngine;
using System.Collections;

public class FinishLine : MonoBehaviour {

	GameObject mPlayer;

	public string mMoneyKey = "Sheriff Money";			//
	public string mHealthKey = "Sheriff Health";		//
	public string mCurHealthKey = "Sherif CurHealth";	//
	public string mPotionsKey = "Sheriff Potions";		//
	public string mLevelKey = "Sheriff Level";	

	float mPlayerMaxHealth;				//
	float mPlayerHealth;					//
	int mPlayerMoney;					//
	int mPlayerPotions;
	int mLevel;

	void Start()
	{
		mPlayer = GameObject.Find ("Player");
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.name == "Player")
		{
			Debug.Log ("VICTOIRE!");

			mPlayerMaxHealth = mPlayer.gameObject.GetComponent<CharacterStats>().mPlayerMaxHealth;
			mPlayerMoney = mPlayer.gameObject.GetComponent<CharacterStats>().mPlayerMoney;
			mPlayerPotions = mPlayer.gameObject.GetComponent<CharacterStats>().mPlayerPotions;
			mPlayerHealth = mPlayer.gameObject.GetComponent<CharacterStats>().mPlayerHealth;
			mLevel = mPlayer.gameObject.GetComponent<CharacterStats>().mLevel;

			mLevel++;

			PlayerPrefs.SetFloat(mHealthKey, mPlayerMaxHealth);
			PlayerPrefs.SetInt(mMoneyKey, mPlayerMoney);
			PlayerPrefs.SetInt(mPotionsKey, mPlayerPotions);
			PlayerPrefs.SetFloat(mCurHealthKey, mPlayerHealth);
			PlayerPrefs.SetInt(mLevelKey, mLevel);

			Camera.main.gameObject.SendMessage("FadeToBlack", SendMessageOptions.DontRequireReceiver);
		}
	}

	void OnDestroy()
	{

	}
}
