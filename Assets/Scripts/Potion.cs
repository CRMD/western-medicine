﻿using UnityEngine;
using System.Collections;

public class Potion : MonoBehaviour {

//	public Light mLight;
	private GameObject mStore;

	public Component mHalo;

	// Use this for initialization
	void Start () {
		if(!mStore)
			mStore = GameObject.Find ("Shop");
//		mLight.active = false;

		mHalo = gameObject.GetComponent ("Halo");
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnMouseOver()
	{
		if(!Camera.main.GetComponent<CameraFollow>().mShopping)
			return;
//		if(Time.timeScale == 0)
//		{
//			mLight.active = true;
			if(Input.GetKeyDown(KeyCode.Mouse0))
			{
				if(mStore.GetComponent<Store>().BuyPotion() == true)
				{
					if (CharacterController3D.instance.potionClickSounds.Length != 0)
						AudioSource.PlayClipAtPoint(CharacterController3D.instance.potionClickSounds [Random.Range (0, CharacterController3D.instance.potionClickSounds.Length - 1)], Camera.main.transform.position);
					else
						Debug.Log("No Potion Click Sounds");

					Destroy(gameObject);
				}
			}
			mHalo.GetType().GetProperty("enabled").SetValue(mHalo, true, null);
//		}
	}

	void OnMouseExit()
	{
//		mLight.active = false;
		mHalo.GetType().GetProperty("enabled").SetValue(mHalo, false, null);
	}

}
