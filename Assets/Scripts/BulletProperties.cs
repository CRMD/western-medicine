﻿using UnityEngine;
using System.Collections;

public class BulletProperties : MonoBehaviour {

	public float mSpeed;
	public float mDamage;
	public float mKillTime;
	public GameObject mOwner;

	public BulletProperties(float spd, float dmg, float kTime, GameObject owner)
	{
		mSpeed = spd;
		mDamage = dmg;
		mKillTime = kTime;
		mOwner = owner;
	}
}
