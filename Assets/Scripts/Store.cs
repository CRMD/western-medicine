﻿using UnityEngine;
using System.Collections;

public class Store : MonoBehaviour {

	public float mCost;					//The base cost of the potions
	public float mCostIncrease;			//How much the cost will increase every time you purchase a potion
	private int mPotionsSold;			//The number of potions that have been sold
	private GameObject mPlayer;			//Reference to the player

	private TextMesh mSign;

	void Start () {
		if(!mPlayer)
		{
			mPlayer = GameObject.Find("Player");
			mSign = gameObject.GetComponentInChildren<TextMesh>();
		}
	}

	// Update is called once per frame
	void Update () {
		mSign.text = "$" + (mCost + (mCostIncrease * mPotionsSold)).ToString();
	}

	public bool BuyPotion() 
	{
		if(mPlayer.GetComponent<CharacterStats>().BuyPotion((int)(mCost + (mCostIncrease * mPotionsSold))))
		{
			mPotionsSold++;
			return true;
		}
		return false;
	}
}
