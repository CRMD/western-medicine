﻿using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("Image Effects/Displacement/Warp")]
public class WarpEffect : ImageEffectBase
{
	public float periods = 0.5F;
	public float radius = 0.5F;
	public float speed = 2.0F;
	public Transform targetObject;
	
	// Called by camera to apply image effect
	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Vector3 centre = Camera.main.WorldToScreenPoint(targetObject.position);
		
		centre.x /= Screen.width;
		centre.y /= Screen.height;
		
		material.SetFloat("_speed", speed);
		material.SetFloat("_radius", radius);
		material.SetFloat("_periods", periods);
		material.SetVector("_centre", centre);
		material.SetFloat("_timeN", Time.timeSinceLevelLoad);
		
		Graphics.Blit(source, destination, material);
	}
}
