﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {
	
	public int mMaxAmmo;						//The maximum amount of ammunition this weapon can hold
	private int mAmmo;							//The current ammunition that the weapon is holding
	public int mClipSize;						//The size of each clip
	private int mCurClip;						//The bullets remaining in the clip

	public bool mInfiniteAmmo;					//Toggle infinite ammo (still requires reloads
	public bool mBottomlessClip;				//Toggle bottomless clip

	public float mDamage;						//The total damage of each shot (will be divided by mShotsPerRound)
	public float mBulletSpeed;					//The speed the bullet travels

	private bool mReloading;					//If the gun is reloading
	public float mReloadTime;					//The amount of time it takes to reload the gun
	private float mCurReload;					//The amount of time left to finish reloading

	private Transform mPlayer;					//The Player Reference
	public GameObject mBullet;					//The GameObject that this gun uses for bullets

	public int mShotsPerRound;					//The number of bullets that are generated each shot (counts as 1 bullet)
	public float mSpread;						//The angle in front of the player that the rounds are spread with each shot

	public float mBulletKillTime;				//How long the bullet lasts before it destroys itself

	private BulletProperties mBulletProperties;	//Passed onto each bullet upon instantiation

	private Transform mPlayerHead;				//Direction player is looking, used for shooting

	public int mReloadAmount = 6;				//The number of bullets that are reloaded at once	

	public Animator mAnima;						//The animator (fires the guns obviously.)
	
	public AudioClip[] fireSounds;
	public AudioClip[] emptySounds;
	public AudioClip[] loadSounds;

	public AudioClip[] bossAttackSounds;
	
	void Start () {
		if(gameObject.name == "Player")
		{
			if(!mPlayerHead)
				mPlayerHead = GameObject.Find("HeroAnimations/SherrifPelvis/SherrifSpine1/SherrifSpine2/SherrifSpine3/SherrifSpine4/SherrifSpine5").transform;
			if(!mPlayerHead)
				mPlayerHead = GameObject.Find("HeroTest2/SherrifPelvis/SherrifSpine1/SherrifSpine2/SherrifSpine3/SherrifSpine4/SherrifSpine5").transform;
			if(!mPlayerHead)
				mPlayerHead = GameObject.Find("Ancient_Knight/mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2").transform;
			if(!mPlayer)
				mPlayer = GameObject.Find("Player").transform;

			
			mBulletProperties = new BulletProperties(mBulletSpeed, (mDamage / mShotsPerRound), mBulletKillTime, gameObject);
		}
		else if(transform.parent.gameObject.name == "Boss")
		{
			if(!mPlayerHead)
				mPlayerHead = GameObject.Find("HucksterGunAnimations").transform;
			if(!mPlayer)
				mPlayer = GameObject.Find("Boss/HucksterGunAnimations/Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base HumanSpine4/Base HumanSpine5").transform;

			mBulletProperties = new BulletProperties(mBulletSpeed, (mDamage / mShotsPerRound), mBulletKillTime, transform.parent.gameObject);
		}
		if(!mBullet)
			Debug.Log("Assign mBullet in inspector");
		mAmmo = mMaxAmmo;
		mCurClip = mClipSize;
	}

	// Update is called once per frame
	void Update () {
		if(mReloading)
		{
			mCurReload -= Time.deltaTime;
			if(mCurReload <= 0)
			{
				if(!mInfiniteAmmo)
				{
					mAmmo -= (mClipSize - mCurClip);
				}
					
				mCurClip += mReloadAmount;
				if(mCurClip > mClipSize)
					mCurClip = mClipSize;

				if(mCurClip != mClipSize)
				{
					mCurReload = mReloadTime;
				}
				else
				{
					mReloading = false;
				}
			}
		}
	}

	public void Reload()
	{
		if(!mReloading && mCurClip != mClipSize)
		{
			mReloading = true;
			mCurReload = mReloadTime;


			Debug.Log(transform.name);

			if (loadSounds.Length != 0)
				AudioSource.PlayClipAtPoint (loadSounds [Random.Range (0, loadSounds.Length - 1)], transform.position);
			else
				Debug.Log("No Fire Audio");
		}
	}

	public void Fire()
	{
		if(mCurClip <= 0)
		{
			Reload();
		}
		if(mCurClip > 0 && !mReloading)
		{
			//Debug.Log(transform.name);
			if(transform.name != "HucksterGunAnimations")
			{
				if (fireSounds.Length != 0)
					AudioSource.PlayClipAtPoint (fireSounds [Random.Range (0, fireSounds.Length - 1)], transform.position);
				else
					Debug.Log("No Fire Audio");

				mAnima.SetInteger ("gun", mAnima.GetInteger("gun") * -1);
				mAnima.SetBool ("shooting", true);
				mAnima.SetInteger("idleType", 0);			// This is here to cancel the head scratch from continuing after interrupting it with shooting.	
			}	
			else
			{
				if (bossAttackSounds.Length != 0)
					AudioSource.PlayClipAtPoint (bossAttackSounds [Random.Range (0, bossAttackSounds.Length - 1)], transform.position);
				else
					Debug.Log("No Boss Fire Audio");
			}
			
			if(!mBottomlessClip)
				mCurClip--;
			if(mShotsPerRound > 1)
			{
				for (int i = 0; i < mShotsPerRound; ++i)
				{
					Vector3 bulletRotation = BulletRotation(new Vector3(mPlayerHead.forward.x, 0.0f, mPlayerHead.forward.z),
					                                        -(mSpread/2) + ((mSpread/(float)mShotsPerRound) * i));
//					Vector3 bulletPos = mPlayerHead.position + bulletRotation;
					//Vector3 bulletPos = new Vector3(mPlayerHead.position.x + mPlayerHead.up.x * 1.1f, mPlayerHead.position.y, mPlayerHead.position.z + mPlayerHead.up.z * 1.1f) + bulletRotation;
					Vector3 bulletPos = mPlayerHead.position;

					if(transform.parent && transform.parent.gameObject.name == "Boss")
					{
						bulletPos += new Vector3(mPlayerHead.forward.x, 0.0f, mPlayerHead.forward.z)*2.0f+BulletRotation(new Vector3(mPlayerHead.forward.x, 0.0f, mPlayerHead.forward.z),
					                                                          -(mSpread/2) + ((mSpread/(float)mShotsPerRound) * i));
					}
					else
					{
						bulletPos += new Vector3(mPlayerHead.forward.z, 0.0f, -mPlayerHead.forward.x)*2.0f+BulletRotation(new Vector3(mPlayerHead.forward.z, 0.0f, -mPlayerHead.forward.x),
						                            -(mSpread/2) + ((mSpread/(float)mShotsPerRound) * i));
					}
					//					(mPlayerHead.position.x + mPlayerHead.up.x * 1.1f, mPlayerHead.position.y, mPlayerHead.position.z + mPlayerHead.up.z * 1.1f)

					Vector3 bulletDir = bulletPos - mPlayerHead.position;

					GameObject bullet = (GameObject)Instantiate(mBullet,
					                                            bulletPos,
					                                            Quaternion.LookRotation(bulletDir));

					float temp = mBulletProperties.mSpeed;
					mBulletProperties.mSpeed += Random.Range(-5.0f, 5.0f);
					bullet.SendMessage("SetValues", mBulletProperties, SendMessageOptions.DontRequireReceiver);
					mBulletProperties.mSpeed = temp;
				}
			}
			else
			{
				Vector3 bulletPos = new Vector3(mPlayerHead.position.x + mPlayerHead.up.x * 4.1f, mPlayerHead.position.y, mPlayerHead.position.z + mPlayerHead.up.z * 4.1f);
				Vector3 bulletDir = bulletPos - mPlayerHead.position;
				GameObject bullet = (GameObject)Instantiate(mBullet,
				                                            bulletPos,
															Quaternion.LookRotation(bulletDir));
				bullet.SendMessage("SetValues", mBulletProperties, SendMessageOptions.DontRequireReceiver);
			}
		}
		else 
		{
			if (emptySounds.Length != 0)
				AudioSource.PlayClipAtPoint (emptySounds [Random.Range (0, emptySounds.Length - 1)], transform.position);
			else
				Debug.Log("No Fire Audio");
		}
	}
	
	private Vector3 BulletRotation(Vector3 vec3, float angle)
	{
		Vector3 retVec;

		retVec.x = vec3.x * Mathf.Cos(Mathf.Deg2Rad * angle) - vec3.z * Mathf.Sin(Mathf.Deg2Rad * angle);
		retVec.z = vec3.x * Mathf.Sin(Mathf.Deg2Rad * angle) + vec3.z * Mathf.Cos(Mathf.Deg2Rad * angle);
		retVec.y = vec3.y;

		return retVec;
	}

	void OnGUI()
	{
		GUI.Label(new Rect(Screen.width - 160.0f, 10.0f, 150.0f, 20.0f), "Revolver: " + mCurClip.ToString() + "/" + mClipSize.ToString() + "/" + mAmmo.ToString() + "/" + mMaxAmmo.ToString());
		if(mReloading)
			GUI.Label(new Rect(Screen.width - 110.0f, 30.0f, 100.0f, 20.0f), "Reloading: " + mCurReload.ToString());
	}
}
